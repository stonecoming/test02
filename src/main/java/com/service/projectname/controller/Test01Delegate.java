package com.service.projectname.controller;

import org.springframework.stereotype.Component;


@Component
public class Test01Delegate {

    public String helloworld(String name){

        // Do Some Magic Here!
        return name;
    }
}
